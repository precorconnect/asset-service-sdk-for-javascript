import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import AddAssetFeature from './addAssetFeature';
import GetAssetsWithIdsFeature from './getAssetsWithIdsFeature';
import ListAssetsWithAccountIdFeature from './listAssetsWithAccountIdFeature';
import SearchForAssetWithSerialNumberFeature from './searchForAssetWithSerialNumberFeature';
import UpdateAccountIdOfAssetsFeature from './updateAccountIdOfAssetsFeature';
import GetAssetsWithProductLineIdsFeature from './getAssetsWithProductLineIdsFeature';
import ListAssetsWithSerialNumberFeature from './listAssetsWithSerialNumberFeature';


/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {AssetServiceSdkConfig} config
     */
    constructor(config:AssetServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(AssetServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddAssetFeature);
        this._container.autoRegister(GetAssetsWithIdsFeature);
        this._container.autoRegister(ListAssetsWithAccountIdFeature);
        this._container.autoRegister(SearchForAssetWithSerialNumberFeature);
        this._container.autoRegister(UpdateAccountIdOfAssetsFeature);
        this._container.autoRegister(GetAssetsWithProductLineIdsFeature);
        this._container.autoRegister(ListAssetsWithSerialNumberFeature);

    }

}
