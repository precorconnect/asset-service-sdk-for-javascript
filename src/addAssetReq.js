export default class AddAssetReq {

    /*
     fields
     */
    _accountId:string;

    _productLineId:number;

    _serialNumber:string;

    _description:string;

    _productLineName:string;

    /**
     *
     * @param {string} accountId
     * @param {number} productLineId
     * @param {string} serialNumber
     * @param {string|null} description
     * @param {string} productLineName
     */
    constructor(accountId:string,
                productLineId:number,
                serialNumber:string,
                description:string,
                productLineName:string
    ) {

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if (!productLineId) {
            throw new TypeError('productLineId required');
        }
        this._productLineId = productLineId;

        if (!serialNumber) {
            throw new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        this._description = description;

        this._productLineName = productLineName;

    }

    /**
     * @returns {string}
     */
    get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {number}
     */
    get productLineId():number {
        return this._productLineId;
    }

    /**
     * @returns {string}
     */
    get serialNumber():string {
        return this._serialNumber;
    }

    /**
     * @returns {string}
     */
    get description():string {
        return this._description;
    }

    /**
     * @returns {string}
     */
    get productLineName():string {
        return this._productLineName;
    }

    toJSON() {
        return {
            accountId: this._accountId,
            productLineId: this._productLineId,
            serialNumber: this._serialNumber,
            description: this._description,
            productLineName: this._productLineName
        };
    }
}