import {inject} from 'aurelia-dependency-injection';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AssetSynopsisView from './assetSynopsisView';

@inject(AssetServiceSdkConfig, HttpClient)
class ListAssetsWithSerialNumberFeature {

    _config:AssetServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AssetServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all assets with the provided serial number
     * @param {string} accountId
     * @param {string} partialSerialNumber
     * @param {string} accessToken
     * @returns {Promise.<AssetSynopsisView[]>}
     */
    execute(accountId:string,partialSerialNumber:string,
            accessToken:string):Promise<Array> {


        return this._httpClient
            .createRequest(`assets`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                accountId: accountId,
                partialSerialNumber: partialSerialNumber
            })
            .send()
            .then((response) =>
                Array.from(
                    response.content,
                    contentItem =>
                        new AssetSynopsisView(
                            contentItem.id,
                            contentItem.productGroupId,
                            contentItem.productGroupName,
                            contentItem.productLineId,
                            contentItem.productLineName,
                            contentItem.serialNumber,
                            contentItem.description,
                            contentItem.productName
                        )
                )
            )
    }
}

export default ListAssetsWithSerialNumberFeature;