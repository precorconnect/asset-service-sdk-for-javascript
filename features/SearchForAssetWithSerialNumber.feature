Feature: Search for asset with serial number
  Searches for an asset with the provided serial number

  Scenario Outline: Success
    Given  I provide a valid accessToken
    And provide a serialNumber that <serialNumber status> by the asset-service
    When I execute searchForAssetWithSerialNumber
    Then <result>

    Examples:
      | serialNumber status | result                |
      | isn't found         | no asset is returned  |
      | is found            | the asset is returned |