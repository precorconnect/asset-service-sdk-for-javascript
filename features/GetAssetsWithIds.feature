Feature: Get Assets With Ids
  Gets the assets with the provided ids

  Scenario: Success
    Given I provide assetIds of existing assets in the asset-service
    And provide a valid accessToken
    When I execute getAssetsWithIds
    Then the assets are returned