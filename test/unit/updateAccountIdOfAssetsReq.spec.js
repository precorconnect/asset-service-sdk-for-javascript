import UpdateAccountIdOfAssetsReq from '../../src/updateAccountIdOfAssetsReq';
import dummy from '../dummy';

/*
 tests
 */
describe('UpdateAccountIdOfAssetsReq class', () => {
    describe('constructor', () => {
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdateAccountIdOfAssetsReq(
                        null,
                        [dummy.assetId]
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');

        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new UpdateAccountIdOfAssetsReq(
                    expectedAccountId,
                    [dummy.assetId]
                );

            /*
             assert
             */
            const actualAccountId =
                objectUnderTest.accountId;

            expect(actualAccountId).toEqual(expectedAccountId);

        });
        it('throws if assetIds is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdateAccountIdOfAssetsReq(
                        dummy.accountId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'assetIds required');

        });
        it('sets assetIds', () => {
            /*
             arrange
             */
            const expectedAssetIds = [dummy.assetIds];

            /*
             act
             */
            const objectUnderTest =
                new UpdateAccountIdOfAssetsReq(
                    dummy.accountId,
                    expectedAssetIds
                );

            /*
             assert
             */
            const actualAssetIds =
                objectUnderTest.assetIds;

            expect(actualAssetIds).toEqual(expectedAssetIds);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new UpdateAccountIdOfAssetsReq(
                    dummy.accountId,
                    [dummy.assetIds]
                );

            const expectedObject =
            {
                accountId: objectUnderTest.accountId,
                assetIds: objectUnderTest.assetIds
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
