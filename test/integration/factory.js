import {AddAssetReq} from '../../src/index';
import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import {AddCommercialAccountReq} from 'account-service-sdk';

export default {
    constructValidAppAccessToken,
    constructValidPartnerRepOAuth2AccessToken,
    constructUniqueAssetSerialNumber,
    constructValidAddCommercialAccountRequest
}

function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'app',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidPartnerRepOAuth2AccessToken(accountId:string = dummy.accountId):string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.userId,
        "account_id": accountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructUniqueAssetSerialNumber() {

    return `${Date.now()}`;

}

function constructValidAddCommercialAccountRequest():AddCommercialAccountReq {

    return new AddCommercialAccountReq(
        dummy.name,
        dummy.postalAddress,
        dummy.phoneNumber,
        dummy.customerType,
        dummy.customerSubType,
        dummy.customerSubSubType,
        dummy.customerBrand,
        dummy.customerSubBrand
    );
}
