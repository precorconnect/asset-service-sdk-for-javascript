import {AddAssetReq} from '../src/index';
import {PostalAddress} from 'postal-object-model';


/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
const dummy = {
    name: 'facilityName',
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    userId: 'fake-email@test.com',
    sapVendorNumber: '0000000000',
    url: 'https://test-url.com',
    //partnerRepId: 1,
    productGroupId : 4,
    productGroupName : 'EFX - Commercial',
    productLineId: 1,
    productLineName: 'AMT',
    customerType:'Clubs',
    customerSubType:'Premium',
    customerSubSubType:'New',
    customerBrand:'Energy',
    customerSubBrand:'Energy',
    assetId: 'assetId',
    assetSerialNumber: 'AB34H19140001',
    accountId: "001A000000zxXUYIA2",
    accountName: 'accountName',
    assetDescription: 'assetDescription',
    postalAddress: new PostalAddress(
        'street',
        'city',
        'WA',
        '98004',
        'US'
    )
};

dummy.addAssetReq =
    new AddAssetReq(
        dummy.accountId,
        dummy.productLineId,
        dummy.assetSerialNumber,
        dummy.assetDescription,
        dummy.productLineName
    );

export default dummy;
